ShopList

Contexte
   ShopList est une application simple pour une utilisation ouverte � tous.
   Avec un design minimaliste elle permettra aux utilisateurs d'avoir la liste des magasins pr�t de chez eux.
   Les utilisateurs peuvent chercher des magasins directement chez eux sans devoir parcourir toute la ville pour les trouver.
   Par exemple si vous venez de d�m�nager dans une nouvelle ville, vous pourrez directement consulter la liste des magasins pr�t de chez vous sans perdre de temps gr�ce aux horaires qui vous permette de ne pas vous ne d�placez pour rien.
   Dans le cas o� un nouveau magasin ouvrirait cette application vous permet � tout moment de l'ajouter � la liste, de le modifier et m�me de le supprim�e pour ainsi permettre de faire passer l'information � tous les utilisateurs.

Le Nuget MahApps.Metro doit �tre install� pour lancer le code dans VisualStudio.

La vid�o de pr�sentation est ici : https://peertube.social/videos/watch/067a58d9-c956-4f71-ab8e-51035ed36b09

Chargement de magasin(s) :
   Si l'on d�cide de charger des magasins alors qu'il n'y en a aucun de charg� on choisi directement le ficher.
   Si des magasins sont d�j� charg�s, on peut alors choisir entre ajout� les magasins � ceux d�j� charg�s ou remplac� la liste actuelle

Persistance :
   Deux fichier de persistance se trouve � la racine sur forge.
   Ceux-ci peuvent �tre charg�s dans l'application.
   Ils contiennent respectivement 2 et 3 magasins.
   Chaque fichier contient Decathlon afin d'avoir un magasin en double.

Diagrammes :
   Les diagrammes sont dans le dossier Diagrammes.
   Le diagramme de cas d'utilisation se trouve dans "Cas d'utilisation.mdj" => "Cas d'utilisation" => "Model" => "Cas d'utilisation"
   Les diagrammes de classe et de paquetage sont dans "Classes & Paquetage.mdj" => "Classe et Paquetage" et respectivement "Classes" => "Classes" et "Paquetage" => "Paquetage"
   Le diagramme de s�quence est dans "S�quence.mdj" => "S�quence" => "S�quence" => "Collaboration1" => "Interaction1" => "S�quence"
   Le diagramme de paquetage de la persistance est inclut dans le diagramme de paquetage
   Le diagramme de classe de la persistance est dans "Classe de la persistance.mdj" => "Classes" => "Persistance"

Fait par Valentin :
-Diagrammes
-Th�me g�n�ral (MahApps.Metro)
-Classes
-Persistance
-IHM

Fait par Ga�than :
-Diagrammes
-Style de bouton
-Binding
-Classes
-IHM
