﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ShopList.Models
{
    public class Person
    {
        private readonly IEnumerable<Shop> shops;

        public List<Shop> Favorite { get; set; }


        [Required]
        public string Email { get; set; }

        public virtual string Pw { get; set; }

        public virtual string Pseudo { get; set; }

        public int Note { get; private set; }

        public Person(string Email, string Pw, string Pseudo, List<Shop> mFavorite)
        {
            this.Email = Email;
            this.Pw = Pw;
            this.Pseudo = Pseudo;
            this.Favorite = mFavorite;
        }

        public void NewFavorite(Shop newShop)
        {
            foreach (Shop var in shops)
            {
                if (newShop == var)
                {
                    Favorite.Add(newShop);
                }
            }
        }
    }
}

