﻿using System;
using System.ComponentModel;

namespace ShopList.Models
{
    /// <summary>
    /// Hours décrit les horraires d'un magasin pour une journée
    /// </summary>
    public class Hours : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String info)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(info));
            }
        }

        /// <summary>
        /// Jour d'ouverture (ex: Lundi)
        /// </summary>
        public string Day { get; set; }

        /// <summary>
        /// Heure d'ouverture le matin
        /// </summary>
        private string hourMorningOpen;

        public string HourMorningOpen
        {
            get
            {
                return hourMorningOpen;
            }
            set
            {
                hourMorningOpen = value;
                OnPropertyChanged("HourMorningOpen");
            }
        }

        /// <summary>
        /// Minutes d'ouverture le matin
        /// </summary>
        private string minuteMorningOpen;

        public string MinuteMorningOpen
        {
            get
            {
                return minuteMorningOpen;
            }
            set
            {
                minuteMorningOpen = value;
                OnPropertyChanged("MinuteMorningOpen");
            }
        }

        /// <summary>
        /// Heure de fermeture le matin
        /// </summary>
        private string hourMorningClose;

        public string HourMorningClose
        {
            get
            {
                return hourMorningClose;
            }
            set
            {
                hourMorningClose = value;
                OnPropertyChanged("HourMorningClose");
            }
        }

        /// <summary>
        /// Minutes de fermeture le matin
        /// </summary>
        private string minuteMorningClose;

        public string MinuteMorningClose
        {
            get
            {
                return minuteMorningClose;
            }
            set
            {
                minuteMorningClose = value;
                OnPropertyChanged("MinuteMorningClose");
            }
        }

        /// <summary>
        /// Heure d'ouverture l'après-midi
        /// </summary>
        private string hourAfternoonOpen;

        public string HourAfternoonOpen
        {
            get
            {
                return hourAfternoonOpen;
            }
            set
            {
                hourAfternoonOpen = value;
                OnPropertyChanged("HourAfternoonOpen");
            }
        }

        /// <summary>
        /// Minutes d'ouverture l'après-midi
        /// </summary>
        private string minuteAfternoonOpen;

        public string MinuteAfternoonOpen
        {
            get
            {
                return minuteAfternoonOpen;
            }
            set
            {
                minuteAfternoonOpen = value;
                OnPropertyChanged("MinuteAfternoonOpen");
            }
        }

        /// <summary>
        /// Heure de fermeture l'après-midi
        /// </summary>
        private string hourAfternoonClose;

        public string HourAfternoonClose
        {
            get
            {
                return hourAfternoonClose;
            }
            set
            {
                hourAfternoonClose = value;
                OnPropertyChanged("HourAfternoonClose");
            }
        }

        /// <summary>
        /// Minutes de fermeture l'après-midi
        /// </summary>
        private string minuteAfternoonClose;

        public string MinuteAfternoonClose
        {
            get
            {
                return minuteAfternoonClose;
            }
            set
            {
                minuteAfternoonClose = value;
                OnPropertyChanged("MinuteAfternoonClose");
            }
        }

        /// <summary>
        /// Constructeur d'une horraire
        /// </summary>
        /// <param name="day">Jour</param>
        /// <param name="hourMorningOpen">Heure d'ouverture le matin</param>
        /// <param name="minuteMorningOpen">Minute d'ouverture le matin</param>
        /// <param name="hourMorningClose">Heure de fermeture le matin</param>
        /// <param name="minuteMorningClose">Minute de fermeture le matin</param>
        /// <param name="hourAfternoonOpen">Heure d'ouverture l'après-midi</param>
        /// <param name="minuteAfternoonOpen">Minute d'ouverture l'après-midi</param>
        /// <param name="hourAfternoonClose">Heure de fermeture l'après-midi</param>
        /// <param name="minuteAfternoonClose">Minute de fermeture l'après-midi</param>
        public Hours(string day, string hourMorningOpen, string minuteMorningOpen, string hourMorningClose, string minuteMorningClose, string hourAfternoonOpen, string minuteAfternoonOpen, string hourAfternoonClose, string minuteAfternoonClose)
        {
            Day = day;
            HourMorningOpen = hourMorningOpen;
            MinuteMorningOpen = minuteMorningOpen;
            HourMorningClose = hourMorningClose;
            MinuteMorningClose = minuteMorningClose;
            HourAfternoonOpen = hourAfternoonOpen;
            MinuteAfternoonOpen = minuteAfternoonOpen;
            HourAfternoonClose = hourAfternoonClose;
            MinuteAfternoonClose = minuteAfternoonClose;
        }

        public override string ToString()
        {
            return $"{Day} : {HourMorningOpen}H{MinuteMorningOpen} - {HourMorningClose}H{MinuteMorningClose} / {HourAfternoonOpen}H{MinuteAfternoonOpen} - {HourAfternoonClose}H{MinuteAfternoonClose}";
        }
    }
}
