﻿using MahApps.Metro.Controls;
using System.Windows;

namespace ShopList.WPF
{
    /// <summary>
    /// Logique d'interaction pour SaveShops.xaml
    /// </summary>
    public partial class SaveShops : MetroWindow
    {
        public SaveShops()
        {
            InitializeComponent();
        }

        private void Button_Validate(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
