﻿using MahApps.Metro.Controls;
using ShopList.Models;
using System.Windows;
using System.Windows.Forms;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;

namespace ShopList.WPF
{
    /// <summary>
    /// Logique d'interaction pour LoadShop.xaml
    /// </summary>
    public partial class LoadShops : MetroWindow
    {
        /// <summary>
        /// Récupère l'instanciation du manager
        /// </summary>
        public Manager Mgr => (Application.Current as App).Mgr;

        public MainWindow Mw;

        public LoadShops()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Charge des magasins en les ajoutants à ceux déjà chargés
        /// </summary>
        private void Button_LoadFileAdd(object sender, RoutedEventArgs e)
        {
            OpenFileDialog loadWindow = new OpenFileDialog();
            loadWindow.Filter = "Text file (*.txt)|*.txt";

            if (loadWindow.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Mgr.ChargerMagasin(loadWindow.FileName);
                this.Close();
            }

            if (Mgr.Doublons)
            {
                MessageBox.Show("/!\\ Certains magasins sont en doublons. Ils n'ont pas été chargés ! /!\\");
                Mgr.Doublons = false;
            }
        }

        /// <summary>
        /// Charge des magasins en remplaçant la liste de magasin déjà chargé
        /// </summary>
        private void Button_LoadFileNew(object sender, RoutedEventArgs e)
        {
            OpenFileDialog loadWindow = new OpenFileDialog();
            loadWindow.Filter = "Text file (*.txt)|*.txt";
            
            if (loadWindow.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Mgr.shops.Clear();
                Mgr.ChargerMagasin(loadWindow.FileName);
                Mw.HomeImage();
                this.Close();
            }

            if (Mgr.Doublons)
            {
                MessageBox.Show("/!\\ Certains magasins sont en doublons. Ils n'ont pas été chargés ! /!\\");
                Mgr.Doublons = false;
            }
        }

        /// <summary>
        /// Annule le chargement de magasins
        /// </summary>
        private void Button_Exit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
