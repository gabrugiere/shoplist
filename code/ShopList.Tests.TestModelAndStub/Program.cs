﻿using ShopList.Models;
using StubLib;
using System;
using System.Linq;

namespace ShopList.Tests.TestModelAndStub
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager Mgr = new Manager(new Stub());
            Mgr.ChargerMagasin(null);
            foreach (Shop mag in Mgr.Shops)
            {
                Console.WriteLine(mag.Name);
                Console.WriteLine(mag.Address.Street);
                Console.WriteLine(mag.Note);
                Console.WriteLine("-----  -----");
            }
            Console.WriteLine("");
            Console.WriteLine("-----  -----");
            Console.WriteLine(Mgr.shops.Count());
            Console.ReadLine();
        }
    }
}