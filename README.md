# ShopList
ShopList est une application permettant d'enregistrer une liste de magasins avec des informations.\
Elle a été réalisée lors de ma première année de DUT Informatique.\
Il s'agit d'une application pour windows réalisé en C# et XAML avec le framework .net de Microsoft.

## 1. Le projet
ShopList est une application simple pour une utilisation ouverte à tous.\
Avec un design minimaliste elle permettra aux utilisateurs d'avoir la liste des magasins prêt de chez eux.\
Les utilisateurs peuvent chercher des magasins directement chez eux sans devoir parcourir toute la ville pour les trouver.\
Par exemple si vous venez de déménager dans une nouvelle ville, vous pourrez directement consulter la liste des magasins prêt de chez vous sans perdre de temps grâce aux horaires qui vous permette de ne pas vous ne déplacez pour rien.\
Dans le cas où un nouveau magasin ouvrirait cette application vous permet à tout moment de l'ajouter à la liste, de le modifier et même de le supprimée pour ainsi permettre de faire passer l'information à tous les utilisateurs.

## 2. Vidéo de présentation
La vidéo de présentation est [ici](https://peertube.social/videos/watch/067a58d9-c956-4f71-ab8e-51035ed36b09).

## 3. Screenshot
[<img src="Screen.png" alt="Capture d'écran de l'application" width=720>](https://gitlab.com/gabrugiere/shoplist/-/blob/master/Screen.png)

## 4. Ce qu'il faut pour l'installer
Le Nuget MahApps.Metro doit être installé pour lancer le code dans VisualStudio.

## 5. Utilisation
### 5.1 Chargement de magasin(s) :
Si l'on décide de charger des magasins alors qu'il n'y en a aucun de chargé on choisi directement le ficher.\
Si des magasins sont déjà chargés, on peut alors choisir entre ajouté les magasins à ceux déjà chargés ou remplacé la liste actuelle

### 5.2 Persistance :
Deux fichier de persistance se trouve à la racine du projet.\
Ceux-ci peuvent être chargés dans l'application.\
Ils contiennent respectivement 2 et 3 magasins.\
Chaque fichier contient Decathlon afin d'avoir un magasin en double.

## Diagrammes
Les diagrammes sont dans le dossier Diagrammes.\
Le diagramme de cas d'utilisation se trouve dans "Cas d'utilisation.mdj" => "Cas d'utilisation" => "Model" => "Cas d'utilisation"\
Les diagrammes de classe et de paquetage sont dans "Classes & Paquetage.mdj" => "Classe et Paquetage" et respectivement "Classes" => "Classes" et "Paquetage" => "Paquetage"\
Le diagramme de séquence est dans "Séquence.mdj" => "Séquence" => "Séquence" => "Collaboration1" => "Interaction1" => "Séquence"\
Le diagramme de paquetage de la persistance est inclut dans le diagramme de paquetage\
Le diagramme de classe de la persistance est dans "Classe de la persistance.mdj" => "Classes" => "Persistance"
